#ifndef __BRX_IR_TX_H__
#define __BRX_IR_TX_H__

#include <stdint.h>
#include <stdbool.h>

#include "BRX_IR_TX_Imp.h"

void BRX_IR_TX_TickHMS(void);

void BRX_IR_TX_QueueSignal(int pulseTickHMSBuffer[], int pulseCount);

bool BRX_IR_TX_Busy(void);

#endif