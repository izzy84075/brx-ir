#ifndef __BRX_IR_RX_STRUCT_H__
#define __BRX_IR_RX_STRUCT_H__

#include <stdint.h>
#include <stdbool.h>

#include "BRX_Data_Structures.h"

#include "BRX_IR_RX_Imp.h"

extern volatile BRX_SIGNATURE_t BRX_IR_RX_finalSignature;
extern volatile bool BRX_IR_RX_finalSignatureReady;

void BRX_IR_RX_NewIRPulseReceived(int pulseLengthTicks, bool IRpresentThisPulse);

#endif