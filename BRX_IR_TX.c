#include "BRX_IR_TX.h"

//Transmitter variables
//Whether or not we're working on sending a signal(Or the delay after a signal)
volatile bool BRX_IR_TX_currentlyTransmitting = false;
//A timer for keeping track of how long until we need to do the next step of a signal
volatile int BRX_IR_TX_txDelayTimerHMS = 0;  
//Our TX buffer
volatile int BRX_IR_TX_txPulseHMS[51];
volatile int BRX_IR_TX_txPulseCount = 0;
//And where we're currently at within that buffer
volatile int BRX_IR_TX_txPulseIndex = 0;
//How long to guarantee between each signal
const int BRX_IR_TX_txPulseCooldownHMS = 100;

void BRX_IR_TX_TickHMS(void) {
    if(BRX_IR_TX_currentlyTransmitting) {
        //If our timer is non-zero
        if(BRX_IR_TX_txDelayTimerHMS) {
            //Subtract one from it
            BRX_IR_TX_txDelayTimerHMS--; 
        } else {
            //Otherwise, do some thinking
            if(BRX_IR_TX_txPulseIndex < BRX_IR_TX_txPulseCount) {
                if(BRX_IR_TX_txPulseIndex % 2 == 0) {
                    //Even index, active IR period
                    BRX_IR_TX_IREnable();
                    BRX_IR_TX_txDelayTimerHMS = BRX_IR_TX_txPulseHMS[BRX_IR_TX_txPulseIndex];
                } else {
                    //Odd index, inactive IR period
                    BRX_IR_TX_IRDisable();
                    BRX_IR_TX_txDelayTimerHMS = BRX_IR_TX_txPulseHMS[BRX_IR_TX_txPulseIndex];
                }
                BRX_IR_TX_txPulseIndex++;
            } else if(BRX_IR_TX_txPulseIndex == BRX_IR_TX_txPulseCount) {
                //End of signal, turn off the IR and do a cooldown
                BRX_IR_TX_IRDisable();
                BRX_IR_TX_txDelayTimerHMS = BRX_IR_TX_txPulseCooldownHMS;
                BRX_IR_TX_txPulseIndex++;
            } else {
                //Cooldown's done, clean up
                BRX_IR_TX_currentlyTransmitting = false;
                BRX_IR_TX_txPulseIndex = 0;
            }
        }
    }
}

void BRX_IR_TX_QueueSignal(int pulseTickHMSBuffer[], int pulseCount) {
    if(!BRX_IR_TX_currentlyTransmitting) {
        for(int i = 0;i < pulseCount;i++) {
            BRX_IR_TX_txPulseHMS[i] = pulseTickHMSbuffer[i];
        }
        BRX_IR_TX_txPulseCount = pulseCount;
        BRX_IR_TX_currentlyTransmitting = true;
    }
}

bool BRX_IR_TX_Busy(void) {
    return BRX_IR_TX_currentlyTransmitting;
}
