#ifndef __BRX_DATA_STRUCTURES_H__
#define __BRX_DATA_STRUCTURES_H__

#include <stdint.h>
#include <stdbool.h>

typedef struct {
    uint32_t dataBits:25;
} BRX_RAW_SIGNATURE_t;

typedef struct {
    uint8_t bulletType:4;
    uint8_t playerID:6;
    uint8_t teamID:2;
    uint8_t damage:8;
    uint8_t unknown:3;
    uint8_t parity:2;
} BRX_TAG_SIGNATURE_t;

typedef struct {
    uint8_t commandFlag:4;
    uint32_t unknown:21;
} BRX_COMMAND_SIGNATURE_t;

typedef struct {
    union {
        BRX_RAW_SIGNATURE_t raw;
        BRX_TAG_SIGNATURE_t tag;
        BRX_COMMAND_SIGNATURE_t command;
    };
    uint8_t bitCount;
} BRX_SIGNATURE_t;

#endif
