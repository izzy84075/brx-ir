#include "BRX_IR_RX_Struct.h"

typedef enum {
    BRX_IR_RX_STATE_IDLE = 0,
    BRX_IR_RX_STATE_DATA_PAUSE,
    BRX_IR_RX_STATE_DATA_BIT,
} BRX_IR_RX_STATES_t;

volatile BRX_IR_RX_STATES_t BRX_IR_RX_state = BRX_IR_RX_STATE_IDLE;

//LTTO decoding variables
volatile BRX_SIGNATURE_t BRX_IR_RX_WIPsignature;

//Finished received LTTO signal variables
volatile BRX_SIGNATURE_t BRX_IR_RX_finalSignature;
volatile bool BRX_IR_RX_finalSignatureReady;

void BRX_IR_RX_NewIRPulseReceived(int pulseLengthTicks, bool IRpresentThisPulse) {
    switch(BRX_IR_RX_state) {
        case BRX_IR_RX_STATE_IDLE:
            //Looking for a ~2ms pre-sync pulse
            if(IRpresentThisPulse && pulseLengthTicks > (1.5*BRX_IR_RX_TICKS_PER_MS) && pulseLengthTicks < (2.5*BRX_IR_RX_TICKS_PER_MS)) {
                //Reset the struct
                BRX_IR_RX_WIPsignature.raw.dataBits = 0;
                BRX_IR_RX_WIPsignature.bitCount = 0;
                //And start capturing. Next thing we're looking for is a data pause.
                BRX_IR_RX_state = BRX_IR_RX_STATE_DATA_PAUSE;
            }
            break;
        case BRX_IR_RX_STATE_DATA_PAUSE:
            //Looking for a ~0.5ms data pause or something longer than ~2.5ms as a sign that either we're done or something got corrupted.
            if(!IRpresentThisPulse && pulseLengthTicks > (0*BRX_IR_RX_TICKS_PER_MS) && pulseLengthTicks < (1*BRX_IR_RX_TICKS_PER_MS)) {
                //Got the pause. Next thing we're looking for is a data bit.
                BRX_IR_RX_state = BRX_IR_RX_STATE_DATA_BIT;
            } else if(!IRpresentThisPulse && (pulseLengthTicks > (2.5*BRX_IR_RX_TICKS_PER_MS)) && BRX_IR_RX_WIPsignature.bitCount == 25) {
                //We're done!
                //Copy things to the "foreground" buffer, so that the gameplay loop can see we got something.
                BRX_IR_RX_finalSignature.raw.dataBits = BRX_IR_RX_WIPsignature.raw.dataBits;
                BRX_IR_RX_finalSignature.bitCount = BRX_IR_RX_WIPsignature.bitCount;
                BRX_IR_RX_finalSignatureReady = true;
                //And reset the receiver state.
                BRX_IR_RX_state = BRX_IR_RX_STATE_IDLE;
            } else {
                //Not what we're looking for. Abort!
                BRX_IR_RX_state = BRX_IR_RX_STATE_IDLE;
            }
            break;
        case BRX_IR_RX_STATE_DATA_BIT:
            //Looking for either a ~0.5ms "0" data bit, or a ~1ms "1" data bit.
            if(IRpresentThisPulse && pulseLengthTicks > (0*BRX_IR_RX_TICKS_PER_MS) && pulseLengthTicks < (1*BRX_IR_RX_TICKS_PER_MS)) {
                //0 bit
                BRX_IR_RX_WIPsignature.raw.dataBits <<= 1;
                BRX_IR_RX_WIPsignature.bitCount += 1;
                //Next thing we're looking for is a data pause.
                BRX_IR_RX_state = BRX_IR_RX_STATE_DATA_PAUSE;
            } else if(IRpresentThisPulse && pulseLengthTicks > (0.5*BRX_IR_RX_TICKS_PER_MS) && pulseLengthTicks < (1.5*BRX_IR_RX_TICKS_PER_MS)) {
                //1 bit
                BRX_IR_RX_WIPsignature.raw.dataBits <<= 1;
                BRX_IR_RX_WIPsignature.raw.dataBits |= 0x01;
                BRX_IR_RX_WIPsignature.bitCount += 1;
                //Next thing we're looking for is a data pause.
                BRX_IR_RX_state = BRX_IR_RX_STATE_DATA_PAUSE;
            } else {
                //Not what we're looking for.
                //Abort!
                BRX_IR_RX_state = BRX_IR_RX_STATE_IDLE;
            }
            break;
    }
}