#ifndef __BRX_IR_RX_H__
#define __BRX_IR_RX_H__

#include <stdint.h>
#include <stdbool.h>

#include "BRX_IR_RX_Imp.h"

extern volatile int BRX_IR_RX_finalPulseHMS[51];
extern volatile int BRX_IR_RX_finalPulseCount;

void BRX_IR_RX_NewIRPulseReceived(int pulseLengthTicks, bool IRpresentThisPulse);

#endif