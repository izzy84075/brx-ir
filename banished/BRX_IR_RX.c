#include "BRX_IR_RX.h"

//LTTO decoding variables
volatile int BRX_IR_RX_WIPpulseHMS[51];
volatile int BRX_IR_RX_WIPpulseCount = 0;

//Finished received LTTO signal variables
volatile int BRX_IR_RX_finalPulseHMS[51];
volatile int BRX_IR_RX_finalPulseCount = 0;

void BRX_IR_RX_NewIRPulseReceived(int pulseLengthTicks, bool IRpresentThisPulse) {
    switch(BRX_IR_RX_WIPpulseCount) {
        case 0:
            //Looking for a ~2ms pre-sync pulse
            if(IRpresentThisPulse && pulseLengthTicks > (1.5*BRX_IR_RX_TICKS_PER_MS) && pulseLengthTicks < (2.5*BRX_IR_RX_TICKS_PER_MS)) {
                BRX_IR_RX_WIPpulseHMS[0] = 4;
                BRX_IR_RX_WIPpulseCount++;
            }
            break;
        case 1:
        case 3:
        case 5:
        case 7:
        case 9:
        case 11:
        case 13:
        case 15:
        case 17:
        case 19:
        case 21:
        case 23:
        case 25:
        case 27:
        case 29:
        case 31:
        case 33:
        case 35:
        case 37:
        case 39:
        case 41:
        case 43:
        case 45:
        case 47:
        case 49:
            //Looking for a ~0.5ms data pause or something longer than ~2.5ms as a sign that either we're done or something got corrupted.
            if(!IRpresentThisPulse && pulseLengthTicks > (0*BRX_IR_RX_TICKS_PER_MS) && pulseLengthTicks < (1*BRX_IR_RX_TICKS_PER_MS)) {
                BRX_IR_RX_WIPpulseHMS[BRX_IR_RX_WIPpulseCount] = 1;
                BRX_IR_RX_WIPpulseCount++;
            } else if(!IRpresentThisPulse && pulseLengthTicks > (2.5*BRX_IR_RX_TICKS_PER_MS)) {
                //We're done!
                //Copy things to the "forground" buffer, so that the gameplay loop can see we got something.
                for(int i = 0;i < BRX_IR_RX_WIPpulseCount;i++) {
                    BRX_IR_RX_finalPulseHMS[i] = BRX_IR_RX_WIPpulseHMS[i];
                }
                BRX_IR_RX_finalPulseCount = BRX_IR_RX_WIPpulseCount;
                //And reset the receiver counter.
                BRX_IR_RX_WIPpulseCount = 0;
            } else {
                //Not what we're looking for. Abort!
                BRX_IR_RX_WIPpulseCount = 0;
            }
            break;
        case 2:
        case 4:
        case 6:
        case 8:
        case 10:
        case 12:
        case 14:
        case 16:
        case 18:
        case 20:
        case 22:
        case 24:
        case 26:
        case 28:
        case 30:
        case 32:
        case 34:
        case 36:
        case 38:
        case 40:
        case 42:
        case 44:
        case 46:
        case 48:
        case 50:
            //Looking for either a ~0.5ms "0" data bit, or a ~1ms "1" data bit.
            if(IRpresentThisPulse && pulseLengthTicks > (0*BRX_IR_RX_TICKS_PER_MS) && pulseLengthTicks < (1*BRX_IR_RX_TICKS_PER_MS)) {
                BRX_IR_RX_WIPpulseHMS[BRX_IR_RX_WIPpulseCount] = 1;
                BRX_IR_RX_WIPpulseCount++;
            } else if(IRpresentThisPulse && pulseLengthTicks > (0.5*BRX_IR_RX_TICKS_PER_MS) && pulseLengthTicks < (1.5*BRX_IR_RX_TICKS_PER_MS)) {
                BRX_IR_RX_WIPpulseHMS[BRX_IR_RX_WIPpulseCount] = 2;
                BRX_IR_RX_WIPpulseCount++;
            } else {
                //Not what we're looking for.
                //Abort!
                BRX_IR_RX_WIPpulseCount = 0;
            }
            break;
        case 51:
            //Looking for a period of silence, at least 2.5ms.
            if(!IRpresentThisPulse && pulseLengthTicks > (2.5*BRX_IR_RX_TICKS_PER_MS)) {
                //We're done!
                //Copy things to the "foreground" buffer, so that the gameplay loop can see we got something.
                for(int i = 0;i < BRX_IR_RX_WIPpulseCount;i++) {
                    finalPulseHMS[i] = BRX_IR_RX_WIPpulseHMS[i];
                }
                finalPulseCount = BRX_IR_RX_WIPpulseCount;
                //And reset the receiver counter.
                BRX_IR_RX_WIPpulseCount = 0;
            } else {
                //Something's wrong. Nothing in the BRX protocol is this long.
                //Abort!
                BRX_IR_RX_WIPpulseCount = 0;
            }
            break;
    }
}
